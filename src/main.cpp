#include <SFML/Graphics.hpp>

int main()
{

    // Create the game window
    sf::RenderWindow window(sf::VideoMode(800, 600), "Arkanoid");
    window.setFramerateLimit(60);
    // Create the paddle shape
    sf::RectangleShape paddle(sf::Vector2f(100.f, 20.f));
    paddle.setFillColor(sf::Color::Green);
    paddle.setPosition(350.f, 580.f);

    // Create the ball shape
    sf::CircleShape ball(10.f);
    ball.setFillColor(sf::Color::Red);
    ball.setPosition(390.f, 550.f);

    // Initialize the ball movement variables
    float ballSpeedX = 3.f;
    float ballSpeedY = -3.f;
    // Create the bricks
    const int numBricks = 48;
    const int bricksPerRow = 12;
    sf::RectangleShape bricks[numBricks];
    sf::Vector2f brickSize(70.f, 20.f);
    sf::Color brickColor(100, 100, 100);
    int brickIndex = 0;
    for (int row = 0; row < 4; ++row) {
        float rowWidth = (bricksPerRow - 1) * (brickSize.x + 5.f); // Calculate the total width of the bricks in the row
        float xOffset = (window.getSize().x - rowWidth) / 2.f; // Calculate the offset to center the bricks horizontally
        for (int col = 0; col < bricksPerRow; ++col) {
            bricks[brickIndex].setSize(brickSize);
            bricks[brickIndex].setFillColor(brickColor);
            float xPosition = col * (brickSize.x + 5.f) + xOffset;
            bricks[brickIndex].setPosition(xPosition, row * (brickSize.y + 5.f) + 50.f);
            ++brickIndex;
        }
    }

    int points = 0;
    bool gameEnded = false;

    // Game loop
    while (window.isOpen() && !gameEnded)
    {
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // Move the paddle with arrow keys
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && paddle.getPosition().x > 0)
            paddle.move(-5.f, 0.f);
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && paddle.getPosition().x + paddle.getSize().x < window.getSize().x)
            paddle.move(5.f, 0.f);

        // Move the ball
        ball.move(ballSpeedX, ballSpeedY);

        // Collision detection with walls
        if (ball.getPosition().x + ball.getRadius() > window.getSize().x || ball.getPosition().x - ball.getRadius() < 0)
            ballSpeedX = -ballSpeedX;
        if (ball.getPosition().y - ball.getRadius() < 0)
            ballSpeedY = -ballSpeedY;

        // Collision detection with paddle
        if (ball.getGlobalBounds().intersects(paddle.getGlobalBounds()))
            ballSpeedY = -ballSpeedY;

        // Collision detection with bricks
        for (int i = 0; i < numBricks; i++) {
            if (bricks[i].getPosition().x != -100.f && bricks[i].getPosition().y != -100.f) {
                if (ball.getGlobalBounds().intersects(bricks[i].getGlobalBounds())) {
                    ballSpeedY = -ballSpeedY;
                    bricks[i].setPosition(-100.f, -100.f); // Hide the brick
                    points++;
                    if (points >= numBricks) {
                        gameEnded = true; // All bricks cleared, end the game
                    }
                }
            }
        }
        // Collision detection with floor
        if (ball.getPosition().y + ball.getRadius() > window.getSize().y) {
            gameEnded = true; // Ball reached the floor, end the game
        }


        // Clear the window
        window.clear();

        // Draw the game objects
        window.draw(paddle);
        window.draw(ball);
        for (int i = 0; i < numBricks; i++) {
            if (bricks[i].getPosition().x != -100.f && bricks[i].getPosition().y != -100.f) {
                window.draw(bricks[i]);
            }
        }

        // Draw the points counter
        sf::Text pointsText;
        sf::Font font;
        if (font.loadFromFile("C:/Users/Marcel Duda/OneDrive/Dokumenty/studia/cpp/arkanoid/src/arial.ttf")) {
            pointsText.setFont(font);
            pointsText.setString("Points: " + std::to_string(points));
            pointsText.setCharacterSize(20);
            pointsText.setFillColor(sf::Color::White);
            pointsText.setPosition(10.f, 10.f);
            window.draw(pointsText);
        }

        // Display the window
        window.display();
    }

    // Game over
    window.clear();
    sf::Text gameOverText;
    sf::Font font;
    if (font.loadFromFile("C:/Users/Marcel Duda/OneDrive/Dokumenty/studia/cpp/arkanoid/src/arial.ttf")) {
        gameOverText.setFont(font);
        gameOverText.setString("Game Over");
        gameOverText.setCharacterSize(50);
        gameOverText.setFillColor(sf::Color::White);
        sf::FloatRect textBounds = gameOverText.getLocalBounds();
        gameOverText.setOrigin(textBounds.width / 2.f, textBounds.height / 2.f);
        gameOverText.setPosition(window.getSize().x / 2.f, window.getSize().y / 2.f);
        window.draw(gameOverText);
        window.display();
    }

    bool waitForRestart = true;
    while (waitForRestart) {
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            else if (event.type == sf::Event::KeyPressed)
            {
                    waitForRestart = false; // Space key pressed, restart the game
            }
        }
    }

    return 0;
}
