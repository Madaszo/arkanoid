# Arkanoid Game

Arkanoid is a classic arcade game where you control a paddle to bounce a ball and break bricks.

## Instructions

1. Use the left and right arrow keys to move the paddle horizontally.
2. The objective is to break all the bricks on the screen using the ball.
3. The ball will bounce off the paddle and walls, and it will destroy bricks upon contact.
4. If the ball falls below the paddle, the game ends.
5. Break all the bricks to win the game!

## Controls

- Left Arrow Key: Move the paddle to the left.
- Right Arrow Key: Move the paddle to the right.
